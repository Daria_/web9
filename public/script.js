$(".center").slick({ //all sliders
	slidesToScroll: 1,
	dots: true,
	centerMode: true,
	centerPadding: "60px",
	slidesToShow: 3,
	arrows: true,
	responsive: [{
		breakpoint: 1500,
		settings: {
			centerMode: true,
			dots: true,
			centerPadding: "40px",
			slidesToShow: 3
		}
	}, {
		breakpoint: 480,
		settings: {
			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1
		}
	}, ]
});
$(".cerf").slick({ //slider for sertificates
	infinite: true,
	slidesToShow: 3,
	slidesToScroll: 1,
	dots: false,
	arrows: false,
	responsive: [{
		breakpoint: 480,
		settings: {
			infinite: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			dots: false,
			arrows: true
		}
	}, ]
});
$(".icon-menu").click(function() { //appearing menu
	$(".nav").animate({
		left: "0px"
	}, 600);
});
$(".ad").click(function() { //appearing menu
	$(".nav").animate({
		left: "-50%"
	}, 600);
});


window.addEventListener('DOMContentLoaded', function() { //catalog
	var button = document.getElementById('but');
	button.addEventListener('click', function(event) {
		$(".dropup-content2").toggle();
	});
});


$(document).ready(function() {
	var position = [45.069536, 38.967404];

	function showGoogleMaps() {
		var latLng = new google.maps.LatLng(position[0], position[1]);
		var mapOptions = {
			zoom: 16,
			streetViewControl: false,
			scaleControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: latLng
		};
		map = new google.maps.Map(document.getElementById("map"), mapOptions);
		marker = new google.maps.Marker({
			position: latLng,
			map: map,
			draggable: false,
			animation: google.maps.Animation.DROP
		});
	}
	google.maps.event.addDomListener(window, "load", showGoogleMaps);
});